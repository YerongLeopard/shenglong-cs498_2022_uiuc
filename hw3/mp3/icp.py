from re import I
import time
import cv2
import numpy as np
import open3d as o3d
import tqdm
import time
import matplotlib.pyplot as plt
from sklearn.neighbors import KDTree
import sys

# Question 4: deal with point_to_plane = True
def fit_rigid(src, tgt, point_to_plane = False):
  '''
    src : 3 x N, coordinates
    tgt : 6 X N, coordinates | normals
  '''
  # Question 2: Rigid Transform Fitting
  # Implement this function
  # -------------------------
  T = np.identity(4)
  centroid_src = np.mean(src.T, axis=0)
  centroid_tgt = np.mean(tgt[:3].T, axis=0)
  AA = src.T - centroid_src
  BB = tgt[:3].T - centroid_tgt

  if not point_to_plane:
    del tgt, src
    H = np.dot(AA.T, BB)
    U, _, Vt = np.linalg.svd(H)
    R = np.dot(Vt.T, U.T)

    if np.linalg.det(R) < 0:
       Vt[2,:] *= -1
       R = np.dot(Vt.T, U.T)

    t = centroid_tgt.T - np.dot(R,centroid_src.T)

    T[:3, :3] = R
    T[:3, 3] = t
  else:
    norms = tgt[3:].T
    n = src.shape[1]
    del tgt, src
    H = np.zeros((n, 6))
    b = np.zeros((n))
    for i in range(n):
      sx, sy, sz = AA[i]

      dx, dy, dz = BB[i]
      nx, ny, nz = norms[i]


      _a1 = (nz * sy) - (ny * sz)
      _a2 = (nx * sz) - (nz * sx)
      _a3 = (ny * sx) - (nx * sy)
      
      _a = np.array([_a1, _a2, _a3, nx, ny, nz])
      _b = (nx * dx) + (ny * dy) + (nz * dz) - (nx * sx) - (ny * sy) - (nz * sz)
      H[i, :] = _a
      b[i] = _b

    tr = np.dot(np.linalg.pinv(H), b)
    T[:3, :3] = np.array(
      [[    1, -tr[2],   tr[1]],
      [ tr[2],      1, -tr[0]],
      [-tr[1], tr[0],       1]]
    )
    T[:3, 3] = tr[3:]

  # -------------------------
  return T

# Question 4: deal with point_to_plane = True
def icp(source, target, init_pose=np.eye(4), max_iter = 20, point_to_plane = False, threshold = 0.1, print_info = True):
  
  src = np.asarray(source.points).T
  tgt = np.vstack((np.asarray(target.points).T, np.asarray(target.normals).T))

  # Question 3: ICP
  # Hint 1: using KDTree for fast nearest neighbour
  # Hint 3: you should be calling fit_rigid inside the loop
  # You implementation between the lines
  # ---------------------------------------------------

  T = init_pose
  transforms = []
  delta_Ts = []

  inlier_ratio = 0
  if print_info: print("iter %d: inlier ratio: %.2f" % (0, inlier_ratio))
  tree = KDTree(tgt[:3].T)
  # THRD = 0.1
  for i in tqdm.tqdm(range(max_iter)):

    T_delta = np.identity(4)

    # ---------------------------------------------------
    T_delta[:] = T
    projected_src = (T @ np.vstack((src, np.ones((1, src.shape[1])))) )[:3 :]

    _ , indices = tree.query(projected_src.T, k=1)
    if i > 0:
      # at least we need one iteraton
      norms = np.linalg.norm(projected_src-tgt[:3, indices.reshape(-1)], axis=0)
      inlier_ratio = sum((norms < threshold).astype(int))/src.shape[1]

    if inlier_ratio > 0.999 and i > 0:
      break
    T_delta = fit_rigid(projected_src, tgt[:, indices.reshape(-1)], point_to_plane)
    T = T_delta @ T
    if print_info : print("iter %d: inlier ratio: %.2f" % (i+1, inlier_ratio))
    # relative update from each iteration
    delta_Ts.append(T_delta.copy())
    # pose estimation after each iteration
    transforms.append(T.copy())
  return transforms, delta_Ts

def rgbd2pts(color_im, depth_im, K):
  # Question 1: unproject rgbd to color point cloud, provide visualiation in your document
  # Your implementation between the lines
  # xyz should be of shape 3 x N
  # color should be of shape 3 x N
  # return 3 D point cloud
  # ---------------------------
  # https://medium.com/yodayoda/from-depth-map-to-point-cloud-7473721d3f
  N = color_im.shape[0] * color_im.shape[1] # todo
  # xyz = np.zeros((3, N))
  # color = np.zeros((3, N))

  # TODO figure out why the matrix inverse does not work
  # uv1 = np.vstack( (np.array(np.unravel_index(np.arange(N), color_im.shape[:2])),
  #   np.ones((1,N))) )
  # K_inv = np.linalg.inv(K)
  # xyz = depth_im.reshape(1, -1) * (K_inv @ uv1)

  # TODO figure out why there is a difference
  color = color_im.reshape(-1, 3).T
  u = range(0, color_im.shape[1])
  v = range(0, color_im.shape[0])

  u, v = np.meshgrid(u, v)
  u = u.astype(float)
  v = v.astype(float)

  Z = depth_im.astype(float)
  X = (u - K[0, 2]) * Z / K[0, 0]
  Y = (v - K[1, 2]) * Z / K[1, 1]

  X = np.ravel(X)
  Y = np.ravel(Y)
  Z = np.ravel(Z)

  xyz = np.vstack((X, Y, Z)) 
  # ---------------------------
  pcd = o3d.geometry.PointCloud()
  pcd.points = o3d.utility.Vector3dVector(xyz.T)
  pcd.colors = o3d.utility.Vector3dVector(color.T)
  return pcd

# TODO (Shenlong): please check that I set this question up correctly, it is called on line 136
def pose_error(estimated_pose, gt_pose):
  # Question 5: Translation and Rotation Error 
  # Use equations 5-6 in https://cmp.felk.cvut.cz/~hodanto2/data/hodan2016evaluation.pdf
  # Your implementation between the lines
  # ---------------------------
  error = {}
  es_R = estimated_pose[:3, :3]
  es_t = estimated_pose[:3, 3]
  gt_R = gt_pose[:3, :3]
  gt_t = gt_pose[:3, 3]
  error['R'] = np.arccos( (np.trace(es_R @ np.linalg.inv(gt_R)) - 1 ) / 2 )
  error['t'] = np.linalg.norm(es_t - gt_t)
  # ---------------------------
  return error

def read_data(ind = 0):
  K = np.loadtxt("data/camera-intrinsics.txt", delimiter=' ')
  depth_im = cv2.imread("data/frame-%06d.depth.png"%(ind),-1).astype(float)
  depth_im /= 1000.  # depth is saved in 16-bit PNG in millimeters
  depth_im[depth_im == 65.535] = 0  # set invalid depth to 0 (specific to 7-scenes dataset)
  T = np.loadtxt("data/frame-%06d.pose.txt"%(ind))  # 4x4 rigid transformation matrix
  color_im = cv2.imread("data/frame-%06d.color.jpg"%(ind),-1)
  # print("data/frame-%06d.color.jpg"%(ind))
  color_im = cv2.cvtColor(color_im, cv2.COLOR_BGR2RGB)  / 255.0
  plt.imshow(color_im)
  # plt.show()  
  return color_im, depth_im, K, T

if __name__ == "__main__":

  # pairwise ICP

  # read color, image data and the ground-truth, converting to point cloud
  color_im, depth_im, K, T_tgt = read_data(0)
  target = rgbd2pts(color_im, depth_im, K)
  # o3d.visualization.draw_geometries([target])

  color_im, depth_im, K, T_src = read_data(40)
  source = rgbd2pts(color_im, depth_im, K)
  # sys.exit(1)
  # downsampling and normal estimatoin
  source = source.voxel_down_sample(voxel_size=0.02)
  target = target.voxel_down_sample(voxel_size=0.02)
  source.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
  target.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.1, max_nn=30))
  # conduct ICP (your code)
  final_Ts, delta_Ts = icp(source, target)
  # final_Ts, delta_Ts = icp(source, target, point_to_plane=True)

  # visualization
  vis = o3d.visualization.Visualizer()
  vis.create_window()
  ctr = vis.get_view_control()

  try:
    ctr.set_front([ -0.11651295252277051, -0.047982289143896774, -0.99202945108647766 ])
    ctr.set_lookat([ 0.023592929264511786, 0.051808635289583765, 1.7903649529102956 ])
    ctr.set_up([ 0.097655832648056065, -0.9860023571949631, -0.13513952033284915 ])
    ctr.set_zoom(0.42199999999999971)
  except:
    print('Did not set view controls')
    pass
  vis.add_geometry(source)
  vis.add_geometry(target)

  save_image = False

  # update source images
  for i in range(len(delta_Ts)):
      source.transform(delta_Ts[i])
      vis.update_geometry(source)
      vis.poll_events()
      vis.update_renderer()
      time.sleep(0.2)
      if save_image:
          vis.capture_screen_image("temp_%04d.jpg" % i)
  # sys.exit()
  # visualize camera
  h, w, c = color_im.shape
  try:
    tgt_cam = o3d.geometry.LineSet.create_camera_visualization(w, h, K, np.eye(4), scale = 0.2)
    src_cam = o3d.geometry.LineSet.create_camera_visualization(w, h, K, np.linalg.inv(T_src) @ T_tgt, scale = 0.2)
    pred_cam = o3d.geometry.LineSet.create_camera_visualization(w, h, K, np.linalg.inv(final_Ts[-1]), scale = 0.2)
  except:
    print('Did not show camera')
  gt_pose = np.linalg.inv(T_src) @ T_tgt
  pred_pose = np.linalg.inv(final_Ts[-1])
  p_error = pose_error(pred_pose, gt_pose)
  print("Ground truth pose:", gt_pose)
  print("Estimated pose:", pred_pose)
  print("Rotation/Translation Error", p_error)
  try:
    tgt_cam.paint_uniform_color((1, 0, 0))
    src_cam.paint_uniform_color((0, 1, 0))
    pred_cam.paint_uniform_color((0, 0.5, 0.5))
  
    vis.add_geometry(src_cam)
    vis.add_geometry(tgt_cam)
    vis.add_geometry(pred_cam)
  except:
    print('Did not show camera')
  vis.run()
  vis.destroy_window()

  # Provide visualization of alignment with camera poses in write-up.
  # Print pred pose vs gt pose in write-up.